# The Natural Selection
The Natural Selection is a Scala library for easily running and experimenting with Genetic algorithms.

Usage:
```
val executor = Executor[T](     // T is the type of the genes your Creatures will be holding (genotype)
    initialPopulation,          // A function that creates an the first generation
    fitnessFunction,            // The fitness function that would be used to evaluate the creatures
    executionGoal,               // The stop condition for the algorithm. Must implement the ExecutionGoal[T] trait
    mutator,                    // A function the mutates Creatures
    selector,                   // A selector Instnce. Must implement the Selection[T] trait
    crossCutPosition,           // The position at which the genes od two creatures are merged, when mated
    printer,                    // Used to print the information for every generation. Must implement GenerationPrinter[T]
    maxPopulation,              // The number of creature to have on each generation
    selectionSize,              // The amount of creatures that should be selected in the Selection phase
    mutationRate                // The probability for every gene of a creature to mutate 
    )
    
val bestCreature = executore.execute()

```

For a full example, see the ``Mastermind`` example in the ```examples``` package.