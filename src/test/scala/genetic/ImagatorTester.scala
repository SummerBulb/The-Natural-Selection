package genetic

import java.awt.Color
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO

import examples.imagator.Runner
import examples.imagator.Shape.ColorMode.{BW, RGBA}
import org.junit.{Assert, Test}

/**
  * Created by SummerBulb on 10/17/2017.
  */

class ImagatorTester {

  @Test
  def compareImages(): Unit = {
    val pureRed = new BufferedImage(200, 200, BufferedImage.TYPE_INT_BGR)

    val graphics = pureRed.getGraphics
    graphics.setColor(Color.RED)
    graphics.fillRect(0, 0, 200, 200)

    val twiceAlpha = new BufferedImage(200, 200, BufferedImage.TYPE_INT_BGR)
    val graphics2 = twiceAlpha.getGraphics
    graphics2.setColor(new Color(255, 0, 0, 127))
    (1 to 2550).foreach(_ => graphics2.fillRect(0, 0, 200, 200))

    ImageIO.write(pureRed, "jpg", new File("pure.jpg"))
    ImageIO.write(twiceAlpha, "jpg", new File("alpha.jpg"))

    Assert.assertEquals(0, Runner.pixelWiseCompare(pureRed, twiceAlpha, RGBA), 0D)
  }

  @Test
  def maxImageDiversity() = {
    val white = new BufferedImage(200, 200, BufferedImage.TYPE_INT_BGR)
    val graphics = white.getGraphics
    graphics.setColor(Color.WHITE)
    graphics.fillRect(0, 0, 200, 200)

    val black = new BufferedImage(200, 200, BufferedImage.TYPE_INT_BGR)

    val graphics2 = black.getGraphics
    graphics2.setColor(Color.BLACK)
    graphics2.fillRect(0, 0, 200, 200)

    Assert.assertEquals(3 * (254 * 254) * 200 * 200L, Runner.pixelWiseCompare(black, black, RGBA), 0D)
  }

  @Test
  def maxImageDiversityBW() = {
    val white = new BufferedImage(200, 200, BufferedImage.TYPE_INT_BGR)
    val graphics = white.getGraphics
    graphics.setColor(Color.WHITE)
    graphics.fillRect(0, 0, 200, 200)

    val black = new BufferedImage(200, 200, BufferedImage.TYPE_INT_BGR)

    val graphics2 = black.getGraphics
    graphics2.setColor(Color.BLACK)
    graphics2.fillRect(0, 0, 200, 200)

    Assert.assertEquals(200*200, Runner.pixelWiseCompare(black, black, BW), 0D)
  }

}
