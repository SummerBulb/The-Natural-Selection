import examples.cube.Runner._

val l = List(1, 2, 3, 4, 5)

l.foldLeft(0)((acc: Int, curr: Int) => acc + curr)


l.foldLeft[Option[Int]](Some(0))((maybeInt: Option[Int], i: Int) => maybeInt match {
  case None => None
  case Some(v) => Some(v+i)
})

1 to 1
1 until 1

3  to (0, -1)
