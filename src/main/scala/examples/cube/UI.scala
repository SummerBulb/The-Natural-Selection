package examples.cube

import java.awt.{Color, Dimension, Polygon}
import java.awt.image.BufferedImage

import javax.swing.ImageIcon
import scalax.chart.api._

import scala.collection.mutable.ArrayBuffer
import scala.swing.event.ButtonClicked
import scala.swing.{Alignment, BoxPanel, Button, Label, MainFrame, Orientation, TextArea}
import scala.util.Random

/**
  * Created by SummerBulb on 10/10/2017.
  */
class UI(targetImage: String) extends MainFrame {

  title = "Hello World"
  private val image = new BufferedImage(800, 800, BufferedImage.TYPE_INT_BGR)
  val graphics = image.getGraphics
  graphics.setColor(Color.WHITE)
  //  graphics.fillRect(20, 20, 50, 50)

  // x is horizontal
  // y is vertical
  //  graphics.drawLine(10,10,60,10)
  //  graphics.drawLine(10,10,10,60)
  //  graphics.drawLine(10,60,60,60)
  //  graphics.drawLine(60,10,60,60)

  val sourceX = 10
  val sourceY = 10
  val cubeSize = 90
  val gap = 3

  val shift = (cubeSize / Math.sqrt(5)).toInt
  val gapShift = (gap / Math.sqrt(5)).toInt

//  drawCubeArray(sourceX, sourceY)
  for ( z <- 0 to 3) {
    drawCubeArray(sourceX + z * (shift + gap), sourceY + z * (shift + gap))

  }
//  drawCubeArray(sourceX + 2 * (shift + gap), sourceY + 2 * (shift + gap))

//  drawCube(10 + 120 + 3,10,120)
//  drawCube(10,10,120)

  private def drawCubeArray(xPos: Int, yPos: Int) = {
    for (x <- 3  to (0, -1);
         y <- 3  to (0, -1)
    ) {
      drawCube(xPos + x * (cubeSize + gap), yPos, cubeSize)
      drawCube(xPos, yPos + y * (cubeSize + gap), cubeSize)
      drawCube(xPos + x * (cubeSize + gap), yPos + y * (cubeSize + gap), cubeSize)

    }
  }

  private def drawCube(x: Int, y: Int, size: Int) = {
    val shift = (size / (Math.sqrt(5))).toInt
    //    val shift = (size/2.2).toInt
    val top = new Polygon(Array(x, x+size, x+size+shift, x+shift), Array(y, y, y+shift, y+shift), 4)
    val left = new Polygon(Array(x, x, x+shift, x+shift), Array(y, y+size, y+size+shift, y+shift),4)

    graphics.setColor(Color.BLUE)
    graphics.fillPolygon(top)
    graphics.setColor(Color.WHITE)
    graphics.drawPolygon(top)
    graphics.setColor(Color.RED)
    graphics.fillPolygon(left)
    graphics.setColor(Color.WHITE)
    graphics.drawPolygon(left)
    graphics.setColor(Color.GREEN)

//    graphics.drawRect(x, y, size, size)
    graphics.fillRect(x + shift, y + shift, size, size)
    graphics.setColor(Color.WHITE)
    graphics.drawRect(x + shift, y + shift, size, size)
/*
    graphics.drawLine(x, y, x + shift, y + shift)
    graphics.drawLine(x, y + size, x + shift, y + shift + size)
    graphics.drawLine(x + size, y, x + shift + size, y + shift)
    graphics.drawLine(x + size, y + size, x + shift + size, y + shift + size)*/
  }


  private val icon = new ImageIcon(image)

  val data = ArrayBuffer[(Int, Double)]()
  val chart = XYLineChart(data)
  var chartComp = chart.toComponent

  val targetImageLabel = new Label("", icon, Alignment.Center)
  val generationBestImageLabel = new Label("", icon, Alignment.Center)
  val globalBestImageLabel = new Label("", icon, Alignment.Center)
  private val width = image.getWidth
  private val height = image.getHeight
  generationBestImageLabel.maximumSize = new Dimension(width, height)
  globalBestImageLabel.maximumSize = new Dimension(width, height)
  targetImageLabel.maximumSize = new Dimension(width, height)
  targetImageLabel.icon = new ImageIcon(image)
  private val run = new Button("Run")

  var generation: Int = 0
  var topFitness = Double.NegativeInfinity

  var generationLabel = new Label(s"Generation $generation")
  var generationInfo = new TextArea("No Information")
  val leftPanel = new BoxPanel(Orientation.Vertical) {
    contents += generationLabel
    contents += generationBestImageLabel
    contents += generationInfo
    contents += run
  }

  val graphBox = new BoxPanel(Orientation.Horizontal) {
    contents += chartComp
  }

  val globalAndTarget = new BoxPanel(Orientation.Horizontal) {
    contents += globalBestImageLabel
    contents += targetImageLabel
  }
  val rightPanel = new BoxPanel(Orientation.Vertical) {
    contents += globalAndTarget
    contents += graphBox
  }

  private val mainPanel = new BoxPanel(Orientation.Horizontal) {
    contents += leftPanel
    // contents += rightPanel
  }
  contents = mainPanel


  listenTo(run)

  reactions += {
    case ButtonClicked(`run`) => {
      incrementGeneration()

      image.getGraphics.fillOval(Random.nextInt(width), Random.nextInt(height), 5, 5)
      generationBestImageLabel.icon = new ImageIcon(image)

      val randomDouble = Random.nextDouble
      addToChart(randomDouble)

      mainPanel.revalidate
      mainPanel.repaint
    }
  }

  private def addToChart(randomDouble: Double) = {
    data.append((data.length, randomDouble))
    graphBox.contents -= chartComp
    chartComp = XYLineChart(data).toComponent
    graphBox.contents += chartComp
  }

  private def incrementGeneration() = {
    generation += 1
    leftPanel.contents -= generationLabel
    generationLabel = new Label(s"Generation $generation")
    leftPanel.contents.prepend(generationLabel)
  }

  def generationBestImage(image: BufferedImage, fitness: Double): Unit = {
    incrementGeneration()
    generationBestImageLabel.icon = new ImageIcon(image)
    addToChart(fitness)
    if (fitness > topFitness) {
      topFitness = fitness
      globalBestImageLabel.icon = new ImageIcon(image)
    }

    generationInfo.text = s"Global Best Fitness: $topFitness\nGeneration Best fitness: $fitness"


    mainPanel.revalidate
    mainPanel.repaint
  }
}

object UIRunner {
  def main(args: Array[String]): Unit = {
    val ui = new UI("dummy")
    ui.visible = true
  }
}