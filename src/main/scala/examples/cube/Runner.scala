package examples.cube

import examples.cube.Runner.{legalFold, cubeSnake}
import genetic.printer.{CubePrinter, GenerationPrinter, StringCreaturePrinter}
import genetic.{Creature, Executor, FitnessGoal, TopRanked}

import scala.annotation.tailrec
import scala.util.Random

/**
  * Created by Shmuel Blitz on 4/12/2018.
  */

class Runner(val callback: FoldCreature => Unit) {
  type Cube[T] = Array[Array[Array[T]]]
  type Pos = (Int, Int, Int)

  Random.setSeed(System.currentTimeMillis)

  val cubeDimension = 4


  val directions = Seq('N', 'S', 'E', 'W', 'U', 'D')

  val printer: GenerationPrinter[Creature[Char], FoldCreature] = CubePrinter(fitnessFunction, callback)

  private val snakeExpectedLength: Double = Math.pow(cubeDimension, 3)
  if (1 + cubeSnake.sum != snakeExpectedLength)
    throw new IllegalArgumentException(s"The input snake has ${cubeSnake.sum} pieces, but should have" +
      s" $snakeExpectedLength pieces")

  val executor = Executor[Char, Creature[Char], FoldCreature](
    randomFold,
    fitnessFunction,
    FitnessGoal[Char, Creature[Char], FoldCreature](snakeExpectedLength, fitnessFunction),
    mutate,
    TopRanked[Char, Creature[Char], FoldCreature](),
    12,
    printer,
    mutationRate = 0.03,
    maxPopulation = 1000,
    selectionSize = 500
  )

  def init() = executor.init()

  def step(steps: Int = 1) = executor.step(steps)

  def goalSatisfied() = executor.goalSatisfied

  def strongestCreature() = executor.strongestCreature

  def weakestCreature() = executor.weakestCreature


  def runExample() = {
    val result = executor.run()

    /*val fold = randomFold()
    println(fitnessFunction(fold))*/
    (cubeSnake, result)
  }

  private def mutate(original: Char): Char = {
    if (directions.contains(original)) {
      original match {
        case 'U' => 'D'
        case 'D' => 'U'
        case 'N' => 'S'
        case 'S' => 'N'
        case 'E' => 'W'
        case 'W' => 'E'
      }
    } else {
      randomCubeDimensionPoint
    }

  }

  private def randomOrthogonalDirection(direction: Char): Char = {
    val relevantDirections = orthogonalDirections(direction)
    relevantDirections(Random.nextInt(relevantDirections.size))
  }

  private def randomFold(): FoldCreature = {
    val startPosition = (1 to 3).map(_ => randomCubeDimensionPoint).toSeq
    val randomFirstDirection = directions(Random.nextInt(6))
    val foldDirections = cubeSnake.tail.foldLeft[Seq[Char]](Seq(randomFirstDirection))((acc, curr) => {
      acc :+ randomOrthogonalDirection(acc.last)
    })
    FoldCreature(startPosition ++ foldDirections)
  }

  private def randomCubeDimensionPoint = {
    (48 + Random.nextInt(cubeDimension)).toChar
  }

  private def orthogonalDirections(orthogonalTo: Char): Seq[Char] = orthogonalTo match {
    case 'E' | 'W' => directions diff Seq('E', 'W')
    case 'N' | 'S' => directions diff Seq('N', 'S')
    case 'U' | 'D' => directions diff Seq('U', 'D')
    case _ => directions
  }


  def fitnessFunction(folds: Seq[Char]): Double = {
    //println(folds)

    if (!legalFold(folds.drop(3))) {
      throw new IllegalStateException("Illegal fold")
    } else {
      val x = folds(0).asDigit
      val y = folds(1).asDigit
      val z = folds(2).asDigit

      val startPosition = (x, y, z)
      val cube = cubePositions(cubeSnake, folds.drop(3), startPosition)
      val noOverlapCube = cube.toSet

      if (cube.size != noOverlapCube.size) {
        println(f"Cube Size: ${cube.size}, NoOverlap Size: ${noOverlapCube.size}")
        0
      } else {
        val (xVals, yVals, zVals) = cube.unzip3

        val minX = xVals.min
        val maxX = xVals.max
        val minY = yVals.min
        val maxY = yVals.max
        val minZ = zVals.min
        val maxZ = zVals.max

        val xRange = Math.abs(maxX - minX)
        val yRange = Math.abs(maxY - minY)
        val zRange = Math.abs(maxZ - minZ)

        val xError = Math.abs(4 - xRange)
        val yError = Math.abs(4 - yRange)
        val zError = Math.abs(4 - zRange)

        val error: Double = xError + yError + zError


        //val a = noOverlapCube.size.toDouble / (xRange * yRange * zRange).toDouble

        val b = 64 / (error / 10 + 1)

        //a * b
        b
      }
    }
  }


  def toCube(folds: Seq[Char]) = {
    val cubeDim = cubeSnake.sum
    val startPos = folds.take(3)
    val x = startPos(0).asDigit
    val y = startPos(1).asDigit
    val z = startPos(2).asDigit

    val startPosition = (x, y, z)
    val initialCube = Array.ofDim[Int](cubeDim, cubeDim, cubeDim)
    initialCube(x)(y)(z) = 1
    val foldDirections = folds.drop(3)
    val cubeFolds = cubeSnake.zip(foldDirections)

    val MaybeFinalCube: Option[Cube[Int]] = foldCube(startPosition, initialCube, cubeFolds)
    MaybeFinalCube
  }

  def cubePositions(snake: Seq[Int], folds: Seq[Char], source: (Int, Int, Int)): Seq[(Int, Int, Int)] = {
    if (snake.length != folds.length) {
      throw new IllegalStateException("The Snake ans the Folds do not have the same length.")
    }

    val snakeFolds = snake.zip(folds)
    val moves = snakeFolds.flatMap((tuple: (Int, Char)) => (1 to tuple._1).map(_ => tuple._2))

    moves.foldLeft(Seq(source))(
      (locations: Seq[(Int, Int, Int)], direction: Char) => locations :+ move(locations.last, direction, 1)
    )


  }

  def foldCube(startPosition: (Int, Int, Int), initialCube: Cube[Int], cubeFolds: Seq[(Int, Char)]): Option[Cube[Int]] = {
    cubeFolds.foldLeft[Option[(Cube[Int], Pos)]](Some((initialCube, startPosition)))(
      (maybeCube: Option[(Cube[Int], (Int, Int, Int))], foldStep: (Int, Char)) => maybeCube match {
        case None => None
        case Some(tuple1) => {
          val cube = tuple1._1
          val pos = tuple1._2
          val length = foldStep._1
          val direction = foldStep._2

          val newPositions: Seq[(Pos)] = (1 to length).map((i: Int) => {
            move(pos, direction, i)
          })

          val xyzValues = newPositions.unzip3
          val minX = xyzValues._1.min
          val minY = xyzValues._2.min
          val minz = xyzValues._3.min

          val moveX = Math.abs(Math.min(0, minX))
          val moveY = Math.abs(Math.min(0, minY))
          val moveZ = Math.abs(Math.min(0, minz))

          newPositions
            .map((pos: Pos) => (pos._1 + moveX, pos._2 + moveY, pos._3 + moveZ))
            .foreach((tuple: (Int, Int, Int)) => {
              val (x, y, z) = tuple
              if (x >= 0 && y >= 0 && z >= 0)
                cube(x)(y)(z) += 1
              else {
                throw new IllegalStateException("Position out of cube scope: " + tuple)
              }


            })
          Some((cube, newPositions.last))
        }
      }
    ).map(tuple => tuple._1)
  }

  def move(location: (Int, Int, Int), direction: Char, distance: Int = 1): (Int, Int, Int) = {
    val (x, y, z) = location
    direction match {
      case 'N' => (x, y + distance, z)
      case 'S' => (x, y - distance, z)
      case 'E' => (x + distance, y, z)
      case 'W' => (x - distance, y, z)
      case 'U' => (x, y, z + distance)
      case 'D' => (x, y, z - distance)
    }
  }


}

object Runner {

  val cubeSnake = Seq(
    2, 1, 2, 1, 1, 3, 1, 2, 1, 2, 1,
    2, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2,
    1, 1, 1, 1, 1, 2, 3, 1, 1, 1, 3,
    1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 1

  )

  @tailrec
  final def legalFold(folds: Seq[Char]): Boolean = {
    if (folds.length == 1) {
      true
    } else {
      val head = folds.head
      val tail = folds.tail
      FoldCreature.areOrthogonal(head, tail.head) && legalFold(tail)
    }

  }


}