package examples.cube

import genetic.{Creature, CreatureLike}
import FoldCreature._

import scala.util.Random

/**
  * Created by Shmuel Blitz on 5/1/2018.
  */
class FoldCreature(val foldDirections: Seq[Char]) extends Creature[Char](foldDirections){

  override def newCreature(newGenes: Seq[Char]): Creature[Char] = {
    FoldCreature(newGenes)
  }

  override def mate(other: Creature[Char], cutPosition: Int): Creature[Char] = {
//  override def mate(other: CreatureLike[Char], cutPosition: Int) = {
    val potentialIndexes = other
      .genes
      .zipWithIndex
      .drop(3)
      .tail
      .filter{ case (direction, index)=> {
        areOrthogonal(direction, genes(index - 1))
      }}.map((tuple: (Char, Int)) => tuple._2) ++ Seq(1,2,3)

    val cutoffPosition = potentialIndexes(Random.nextInt(potentialIndexes.size))

    val fromThis = genes.take(cutoffPosition)
    val fromOther = other.genes.drop(cutoffPosition)
    val creature = FoldCreature(fromThis ++ fromOther)
    if (!Runner.legalFold(creature.genes.drop(3)))
      println(
        """
          +===============+
          I               I
          I    ILLEGAL    I
          I               I
          I     MATE      I
          I               I
          +===============+
        """)
      creature
  }


}

object FoldCreature {
  def apply(foldDirections: Seq[Char]): FoldCreature = new FoldCreature(foldDirections)

  def areOrthogonal(left: Char, right: Char): Boolean = {
    left match {
      case 'E' | 'W' => right != 'E' && right != 'W'
      case 'N' | 'S' => right != 'N' && right != 'S'
      case 'U' | 'D' => right != 'U' && right != 'D'
    }
  }
}
