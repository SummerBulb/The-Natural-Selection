package examples.graphs

import java.time.Duration

import genetic._
import genetic.printer.{GenerationPrinter, IntCreaturePrinter}

import scala.util.Random

class BipartiteMinimumCover(val edges: Seq[(Int, Int)], val subsetA: Seq[Int], val subsetB: Seq[Int]) {

  edges foreach println

  private val dim: Int = subsetA.length + subsetB.length
  val adjMatrix = Array.ofDim[Int](dim, dim)

  edges.foreach((tuple: (Int, Int)) => adjMatrix(tuple._1 - 1)(tuple._2 - 1) = 1)

  print(adjMatrix.map(_.mkString).mkString("\n"))

  def subsetDistance(left: Seq[Int], right: Seq[Int]) = {
    left.zip(right).map((tuple: (Int, Int)) => tuple._1 != tuple._2)
      .count(same => same)
  }

  private def fitness(genes: Seq[Int]): Int = {

    val vertASubsetSelected = genes
      .zipWithIndex
      .filter { case (selected: Int, index: Int) => selected == 1 }
      .map(_._2 + 1)
      .toSet
    val coveredVertexes = vertASubsetSelected
      .flatMap((i:Int) => edges.filter { case (v1, v2) => v1 == i }.map(_._2))

    println(s"Covered verticies: ${coveredVertexes.mkString(",")}")

    if (coveredVertexes.equals(subsetB.toSet))
      1 + subsetA.length - genes.sum
    else
      0
  }

  def minimumCoverA() = {
    val start = System.currentTimeMillis()

    val executor = new Executor[Int, Creature[Int], Creature[Int]](
      () => Creature(subsetA.map((i: Int) => Random.nextInt(2))),
      fitness,
      GenerationAgeGoal[Int, Creature[Int], Creature[Int]](1),
      mutate,
      FitnessWeightedRandom[Int, Creature[Int], Creature[Int]](),
      edges.length / 2,
      IntCreaturePrinter(fitness)
    )

    val bestCreature = executor.run()

    val end = System.currentTimeMillis()

    println(s"Done in ${executor.generationAge} generations.")
    val duration = Duration.ofMillis(end - start)
    println(s"Execution took ${duration}")

    bestCreature
  }

  def mutate(gene: Int) = {
    if (gene == 0)
      1
    else
      0
  }


}

object BipartiteMinimumCover {
  def main(args: Array[String]): Unit = {

    val subsetA = Seq.range(1, 6)
    val subsetB = Seq.range(6, 11)

    /*
        val edges = subsetA.flatMap((vert: Int) => {
          val edgeCount = Random.nextInt(100)
          Range(0, edgeCount).map((i: Int) => (i, Random.nextInt(500) + 500))
        })
    */

    val edges = Seq(
      (1, 6), (1, 7),
      (2, 7), (2, 9),
      (3,6),(3, 7), (3, 8), (3, 9), (3,10),
      (4, 6), (4, 8),
      (5, 7), (5, 10)
    )

    val bipartiteMinimumCover = new BipartiteMinimumCover(edges, subsetA, subsetB)

    val solution = bipartiteMinimumCover.minimumCoverA()

    println
    println(solution)
  }
}
