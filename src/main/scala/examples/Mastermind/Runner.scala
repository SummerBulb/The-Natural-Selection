package examples.Mastermind

import java.time.Duration

import genetic.printer.StringCreaturePrinter
import genetic.{Creature, Executor, FitnessGoal, TopRanked}

import scala.util.Random

/**
  * Created by SummerBulb on 9/19/2017.
  */
object Runner {
//  val goal = "the journey of a thousand miles begins with one step"
    val goal = "i am happy to join with you today in what will go down in history as the greatest demonstration for freedom in the history of our nation"
  //  val goal = "Israel"

  def matchingCharacters(features: Seq[Char]): Double = {
    val matching = features.zip(goal).count((tuple: (Char, Char)) => tuple._1 == tuple._2)
    matching / goal.length.toDouble

    // Math.pow(matching, 10)
  }

  def averageString(strings: Seq[Creature[Char]]): Creature[Char] = {
    val genes = strings
      .map(creature => {
        creature.genes
          .map {
            case ' ' => 123
            case c: Char => c.toInt
          }
      })
      .transpose
      .map((ints: Seq[Int]) => ints.sum / ints.size)
      .map{
        case 123 => ' '
        case i:Int => i.toChar
      }
    Creature[Char](genes)
  }

  def stamMathFunction(features: Seq[Double]) = {
    (features.sum - (math.pow(features(4), 2)) / (features(7)) * features(12))
  }

  def creatureDiff(creature1: Creature[Char], creature2: Creature[Char]): Double = {
    charDiff(creature1.genes, creature2.genes)
  }

  val lowerCaseCharacters = 'a' to 'z'
  val lowerCaseWithSpace = lowerCaseCharacters :+ ' '

  val atoi = lowerCaseWithSpace.zipWithIndex.toMap

  def toInt(c: Char) = c match {
    case ' ' => 123
    case _ => c.toInt
  }

  private def charDiff(genes1: Seq[Char], genes2: Seq[Char]) = {
    //    lazy val c1 = genes1.map(reverLookup)
    //    lazy val c2 = genes2.map(reverLookup)

    var sum = 0D
    for (i <- 0 until genes1.length) {
      val g1 = genes1.apply(i)
      val g2 = genes2(i)
      val x = toInt(g1) - toInt(g2)
      sum += x * x
    }

    Math.sqrt(sum)

    //new EuclideanDistance().compute(c1, c2)
  }

  def maxCharDistance(size: Int): Double = {
    val from = (1 to size).map(_ => 'a')
    val to = (1 to size).map(_ => ' ')

    charDiff(from, to)
  }

  def main(args: Array[String]): Unit = {
    val start = System.currentTimeMillis()
    val maxPopulation = 100
    val selectionSize = 50
    val crossCutPosition = Random.nextInt(goal.size - 2)
    val mutationRate = 0.1 / goal.size


    val executor = Executor[Char, Creature[Char], Creature[Char]](
      () => randomCharacters(goal.length),
      matchingCharacters,
      FitnessGoal[Char, Creature[Char], Creature[Char]](1, matchingCharacters),
      randomCharacter,
      //      FitnessWeightedRandom[Char](),
                  TopRanked[Char, Creature[Char], Creature[Char]](),
//      DiverseTopRanked(creatureDiff, averageString, 1, maxCharDistance(goal.size)),
      crossCutPosition,
      StringCreaturePrinter(matchingCharacters),
      maxPopulation,
      selectionSize,
      mutationRate)

    val bestCreature = executor.run()

    val end = System.currentTimeMillis()

    println(s"Done in ${executor.generationAge} generations.")
    val duration = Duration.ofMillis(end-start)
    println(s"Execution took ${duration}")
  }


  private def randomCharacters(length: Int) = {
    Creature(Random.nextString(length).map(c => lowerCaseWithSpace(c % 27)).toCharArray)
  }

  private def randomDoubles(length: Int) = {
    (1 to length).map(_ => Random.nextDouble() * 100)
  }

  private def randomCharacter(gene:Char): Char = {
    Random.nextString(1)
      .map(c => lowerCaseWithSpace(c % 27))
      .toCharArray
      .head
  }


}