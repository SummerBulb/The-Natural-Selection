package examples.imagator

/**
  * Created by Shmuel Blitz on 11/12/2017.
  */
object Utils {
  def limitRange(i: Int, min: Int, max: Int) = {
    Math.max(Math.min(i, max), min)
  }

  def colorRange(color: Int) = {
    Utils.limitRange(color, 0, 255)
  }
}
