package examples.imagator

import java.awt.image.ColorModel
import java.awt.{Color, Polygon}

import examples.imagator.Shape.ColorMode
import examples.imagator.Shape.ColorMode.{BW, RGBA}

import scala.util.Random

/**
  * Created by Shmuel Blitz on 11/12/2017.
  */
case class Shape(polygon: Polygon, color: Int) {

  def changeColor(colorStepSize:Int, colorMode: ColorMode.EnumVal): Shape = {

    val newColor = colorMode match {
      case RGBA => {
        val orgColor = new Color(this.color, true)
        val red = Utils.colorRange(orgColor.getRed + (colorStepSize - Random.nextInt(2 * colorStepSize + 1)))
        val green = Utils.colorRange(orgColor.getGreen + (colorStepSize - Random.nextInt(2 * colorStepSize + 1)))
        val blue = Utils.colorRange(orgColor.getBlue + (colorStepSize - Random.nextInt(2 * colorStepSize + 1)))
        val alpha = Utils.colorRange(orgColor.getAlpha + (colorStepSize - Random.nextInt(2 * colorStepSize + 1)))

        new Color(red, green, blue, alpha)
      }
      case BW => if (color == Color.BLACK.getRGB) Color.WHITE else Color.BLACK
    }

    Shape(polygon, newColor.getRGB)
  }


  def removePoint(): Shape = {
    val point = Random.nextInt(polygon.npoints)
    point match {
      case 0 => this
      case _ =>
        val oldXpoints = polygon.xpoints
        val xPoints = oldXpoints.take(point) ++ oldXpoints.drop(point + 1)
        val oldYpoints = polygon.ypoints
        val yPoints = oldYpoints.take(point) ++ oldYpoints.drop(point + 1)
        Shape(new Polygon(xPoints, yPoints, xPoints.size), color)
    }
  }

  def addPoint(pointStepSize: Int, width: Int, height: Int): Shape = {
    println(s"Adding point. Current: ${polygon.npoints} edges.")
    val point = Random.nextInt(polygon.npoints)

    val xPoint = Utils.limitRange(polygon.xpoints(point) + (pointStepSize - Random.nextInt(2 * pointStepSize + 1)), 0, width)
    val yPoint = Utils.limitRange(polygon.ypoints(point) + (pointStepSize - Random.nextInt(2 * pointStepSize + 1)), 0, height)
    val xPoints = polygon.xpoints.take(point) ++ (xPoint +: polygon.xpoints.drop(point))
    val yPoints = polygon.ypoints.take(point) ++ (yPoint +: polygon.ypoints.drop(point))


    val newPolygon = new Polygon(xPoints, yPoints, xPoints.size)
    println(s"New polygon has ${newPolygon.npoints} edges.")
    Shape(newPolygon, color)
  }

  def movePoint(moveSize: Int, width: Int, height: Int): Shape = {
    val point = Random.nextInt(polygon.npoints)
    val xPoints = polygon.xpoints
    val yPoints = polygon.ypoints
    xPoints(point) = Utils.limitRange(xPoints(point) + (moveSize - Random.nextInt(2 * moveSize + 1)), 0, width)
    yPoints(point) = Utils.limitRange(yPoints(point) + (moveSize - Random.nextInt(2 * moveSize + 1)), 0, height)

    Shape(new Polygon(xPoints, yPoints, xPoints.size), color)
  }


}

object Shape {

  object ColorMode {
    sealed trait EnumVal
    case object BW extends EnumVal
    case object RGBA extends EnumVal
  }


  def randomShape(IMG_WIDTH: Int, IMG_HEIGHT: Int, polygonEdges: Int, colorMode: ColorMode.EnumVal): Shape = {
    val xPoints = (1 to polygonEdges).map(_ => Random.nextInt(IMG_WIDTH)).toArray
    val yPoints = (1 to polygonEdges).map(_ => Random.nextInt(IMG_HEIGHT)).toArray
    val newColor = colorMode match {
      case RGBA =>
        new Color(Random.nextInt(256), Random.nextInt(256), Random.nextInt(256), Random.nextInt(256))
      case BW => if (Random.nextBoolean()) Color.BLACK else Color.WHITE
    }
    Shape(new Polygon(xPoints, yPoints, xPoints.size),   newColor.getRGB)
  }
}
