package examples.imagator

import java.awt.image.BufferedImage
import java.awt.{Color, Polygon}
import java.io.File
import java.nio.file.{Files, Paths}
import javax.imageio.ImageIO

import examples.imagator.Shape.ColorMode
import examples.imagator.Shape.ColorMode.{BW, RGBA}
import genetic._
import genetic.printer.ImageCreatureUIPrinter

import scala.util.Random

/**
  * Created by SummerBulb on 10/1/2017.
  */
object Runner {

  def pixelWiseCompare(targetImage: BufferedImage, newImage: BufferedImage, colorMode: ColorMode.EnumVal) = {
    val width = newImage.getWidth
    val height = newImage.getHeight

    var distance = 0d

    for (x <- 0 until width) {
      for (y <- 0 until height) {
        val c1 = new Color(targetImage.getRGB(x, y))
        val c2 = new Color(newImage.getRGB(x, y))
        //add the pixel fitness to the total fitness ( lower is better )
        distance += colorDistance(c1, c2, colorMode)
      }
    }
    distance
  }

  def colorDistance(c1: Color, c2: Color, colorMode: ColorMode.EnumVal) = {
    colorMode match {
      case RGBA => {
        //get delta per color
        val deltaRed = c1.getRed - c2.getRed
        val deltaGreen = c1.getGreen - c2.getGreen
        val deltaBlue = c1.getBlue - c2.getBlue

        // measure the distance between the colors in 3D space
        deltaRed * deltaRed +
          deltaGreen * deltaGreen +
          deltaBlue * deltaBlue
      }
      case BW => if (c1.equals(c2)) 0 else 1
    }
  }

  def cropCompare(targetImage: BufferedImage, newImage: BufferedImage, colorMode: ColorMode.EnumVal) = {
    val height = targetImage.getHeight
    val width = targetImage.getWidth
    val subImageTarget = targetImage.getSubimage(0, 0, height / 2, width / 2)
    val subImageNew = newImage.getSubimage(0, 0, height / 2, width / 2)

    pixelWiseCompare(subImageTarget, subImageNew, colorMode)
  }

  def randomPixelsCompare(targetImage: BufferedImage, newImage: BufferedImage, colorMode: ColorMode.EnumVal) = {
    val width = targetImage.getWidth
    val height = targetImage.getHeight

    def pixelDistance(x: Int, y: Int) = {
      val c1 = new Color(targetImage.getRGB(x, y))
      val c2 = new Color(newImage.getRGB(x, y))
      colorDistance(c1, c2, colorMode)
    }

    val randomPixelsDistance = Range(0, 10).map(_ => {
      val x = Random.nextInt(width)
      val y = Random.nextInt(height)
      pixelDistance(x, y)
    }).sum

    val pixelGrid = for(x <- Range(0, width-1, 30); y <- Range(0, height-1, 30)) yield (x,y)
    val fixedPixelsDistnace =   pixelGrid
      .map(tuple => {
        pixelDistance(tuple._1, tuple._2)
      })
      .sum

    randomPixelsDistance + fixedPixelsDistnace
  }


  def main(args: Array[String]): Unit = {
    if (args.length != 1) {
      println("Argument missing.\nPlease provide an output base directory (e.g. your desktop path) as the first input.")
      sys.exit(1)
    }

    val basePath = args(0)
    val polygonsQuantity = 50
    val edges = 4
    val path = basePath + "\\target.jpg"

    runGenetic(basePath, polygonsQuantity, edges)
  }

  private def runGenetic(basePath: String, polygonsQuantity: Int, edges: Int) = {
    val targetImage = ImageIO.read(new File(this.getClass.getResource("/star.jpg").getPath))
    //    val targetImage = ImageIO.read(new File(this.getClass.getResource("/RGBW.jpg").getPath))
    //    val targetImage = ImageIO.read(new File(this.getClass.getResource("/sw400_400.jpg").getPath))
    //    val targetImage = ImageIO.read(new File(s"$basePath\\target.jpg"))
    val width = 200
    //    val width = 400
    val factor = targetImage.getWidth / width.toDouble
    val height = (targetImage.getHeight / factor).floor.toInt
    val targetImageResized = new BufferedImage(width, height, targetImage.getType)
    val graphics = targetImageResized.createGraphics
    graphics.drawImage(targetImage, 0, 0, width, height, null)

    val sampleSize = 35

    val IMG_WIDTH = targetImageResized.getWidth
    val IMG_HEIGHT = targetImageResized.getHeight
    val colorMode: ColorMode.EnumVal = ColorMode.BW

    val fitness = randomPixelsCompare(targetImageResized, targetImageResized, colorMode)

    val targetPath = s"$basePath\\target_small.jpeg"
    ImageIO.write(targetImageResized, "jpg", new File(targetPath))

    val ui = new UI(targetPath)
    ui.visible = true

    println(s"Fitness: $fitness")

    val maxDistance = colorMode match {
      case BW => 3 * sampleSize //IMG_WIDTH * IMG_HEIGHT
      case RGBA => 3L * (254 * 254) *  /* IMG_WIDTH * IMG_HEIGHT */ sampleSize * 3
    }

    def firstGeneration() = {
      val realShapes = (1 to 10).map(_ => Shape.randomShape(IMG_WIDTH, IMG_HEIGHT, edges, colorMode))
      val emptyShapes = (1 to polygonsQuantity - 10).map(_ => Shape.randomShape(IMG_WIDTH, IMG_HEIGHT, 1, colorMode))

      Creature(realShapes ++ emptyShapes)
    }

    def fitnessFunction(genes: Seq[Shape]) = {
      val image = generateImage(genes, IMG_WIDTH, IMG_HEIGHT)
      val edgeCount = genes.map(_.polygon).map(_.npoints).sum
      (maxDistance - randomPixelsCompare(targetImageResized, image, colorMode)) //- (edgeCount * 100)
      //      (maxDistance - pixelWiseCompare(targetImageResized, image)) / (3 * Math.log1p(edgeCount))
    }

    def mutatorFunction(gene: Shape) = randomPolygon(gene, IMG_WIDTH, IMG_HEIGHT, edges, colorMode)

    def imgGenerator(polygons: Seq[Shape]) = generateImage(polygons, IMG_WIDTH, IMG_HEIGHT)

    def imagesDistance(source: Creature[Shape], destination: Creature[Shape]): Double = {
      val sourceImage = generateImage(source.genes, IMG_WIDTH, IMG_HEIGHT)
      val destImage = generateImage(destination.genes, IMG_WIDTH, IMG_HEIGHT)
      randomPixelsCompare(sourceImage, destImage, colorMode)
    }


    val outputPath = basePath + File.separator + "imagator_" + System.currentTimeMillis
    Files.createDirectory(Paths.get(outputPath))
    val printer = ImageCreatureUIPrinter[Shape](fitnessFunction, outputPath, imgGenerator, ui, 1)


    val executor = Executor[Shape, Creature[Shape], Creature[Shape]](
      firstGeneration,
      fitnessFunction,
      GenerationAgeGoal(2000000),
      mutatorFunction,
      //      TopRanked[Shape](),
      //      FitnessWeightedRandom[Shape](),
      DiverseTopRanked(imagesDistance, averageImage, maxDistance, maxDistance),
      polygonsQuantity / 2,
      printer,
      30,
      29,
      0.1 / polygonsQuantity
    )

    val bestImage = executor.run()


    ImageIO.write(
      generateImage(bestImage.genes, IMG_WIDTH, IMG_HEIGHT),
      "jpg",
      new File(outputPath + File.separator + "best.jpg"))

    println(s"Done in ${executor.generationAge} generations.")
  }

  def averageImage(images: Seq[Creature[Shape]]): Creature[Shape] = {
    val avgGenes = images.map(_.genes)
      .transpose
      .map((shapes: Seq[Shape]) => {
        val polygons = shapes.map(_.polygon)
        val colors = shapes.map(_.color)
        val maxPolygons = polygons.maxBy(_.npoints).npoints
        val newXpoints = polygons
          .map(pol => pol.xpoints.padTo(maxPolygons, pol.xpoints.last)) //Fill to maxPolygons to allow transpose
          .transpose
          .map(xs => xs.sum / xs.size)
          .toArray
        val newYpoints = polygons
          .map(pol => pol.ypoints.padTo(maxPolygons, pol.ypoints.last))
          .transpose
          .map(ys => ys.sum / ys.size)
          .toArray
        val avgPolygon = new Polygon(newXpoints, newYpoints, newXpoints.length)

        val colorTuples = colors
          .map { case num =>
            val clr = new Color(num, true)
            (clr.getRed, clr.getGreen, clr.getBlue, clr.getAlpha)
          }

        val newRed = colorTuples.map(_._1).sum / colors.length
        val newGreen = colorTuples.map(_._2).sum / colors.length
        val newBlue = colorTuples.map(_._3).sum / colors.length
        val newAlpha = colorTuples.map(_._4).sum / colors.length
        val avgColor = new Color(newRed, newGreen, newBlue, newAlpha)
        Shape(avgPolygon, avgColor.getRGB)
      })
    Creature[Shape](avgGenes)
  }

  var cache = Map[Seq[Shape], BufferedImage]()

  def generateImage(polygons: Seq[Shape], width: Int, height: Int): BufferedImage = {

    cache.get(polygons).getOrElse {

      val newImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB)
      val g = newImage.createGraphics

      polygons.foreach { case Shape(polygon: Polygon, color: Int) =>
        val c = new Color(color, true)
        g.setColor(c)
        g.fillPolygon(polygon)
      }
      g.dispose()
      if (cache.size > 1000) {
        cache = Map(polygons -> newImage)
      } else {
        cache = cache ++ Map(polygons -> newImage)
      }
      newImage
    }
  }


  object Mutation {
    val RANDOM = 0
    val MOVE_POINT = 1
    val ADD_POINT = 2
    val REMOVE_POINT = 3
    val CHANGE_COLOR = 4
  }

  private def randomPolygon(gene: Shape, IMG_WIDTH: Int, IMG_HEIGHT: Int, polygonEdges: Int, colorMode: ColorMode.EnumVal): Shape = {
    import Mutation._
    val pointStepSize = 30
    val colorStep = 10

    Random.nextInt(5) match {
      case RANDOM => Shape.randomShape(IMG_WIDTH, IMG_HEIGHT, polygonEdges, colorMode)
      case MOVE_POINT => gene.movePoint(pointStepSize, IMG_WIDTH, IMG_HEIGHT)
      case ADD_POINT => gene.addPoint(pointStepSize, IMG_WIDTH, IMG_HEIGHT)
      case REMOVE_POINT => gene.removePoint()
      case CHANGE_COLOR => gene.changeColor(colorStep, colorMode)
      case _ => gene
    }
  }


  def resize(inputDir: String, outputDir: String, resizeFactor: Int): Unit = {
    new File(inputDir)
      .listFiles()
      .map((file: File) => {
        val inputImage = ImageIO.read(file)
        val IMG_WIDTH = inputImage.getWidth / resizeFactor
        val IMG_HEIGHT = inputImage.getHeight / resizeFactor


        val resizedImage = new BufferedImage(IMG_WIDTH, IMG_HEIGHT, inputImage.getType)
        val g = resizedImage.createGraphics
        g.drawImage(inputImage, 0, 0, IMG_WIDTH, IMG_HEIGHT, null)
        ImageIO.write(resizedImage, "jpg", new File(outputDir + "\\" + file.getName))
      })
  }
}