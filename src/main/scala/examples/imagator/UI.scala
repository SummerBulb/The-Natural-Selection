package examples.imagator

import java.awt.Dimension
import java.awt.image.BufferedImage
import javax.swing.ImageIcon

import scala.collection.mutable.ArrayBuffer
import scala.swing.event.ButtonClicked
import scala.swing.{Alignment, BoxPanel, Button, Label, MainFrame, Orientation, TextArea}
import scala.util.Random
import scalax.chart.api._

/**
  * Created by SummerBulb on 10/10/2017.
  */
class UI(targetImage: String) extends MainFrame {

  title = "Hello World"
  private val image = ImageUtils.openAndResize(targetImage, 1)
  private val icon = new ImageIcon(image)

  val data = ArrayBuffer[(Int, Double)]()
  val chart = XYLineChart(data)
  var chartComp = chart.toComponent

  val targetImageLabel = new Label("", icon, Alignment.Center)
  val generationBestImageLabel = new Label("", icon, Alignment.Center)
  val globalBestImageLabel = new Label("", icon, Alignment.Center)
  private val width = image.getWidth
  private val height = image.getHeight
  generationBestImageLabel.maximumSize = new Dimension(width, height)
  globalBestImageLabel.maximumSize = new Dimension(width, height)
  targetImageLabel.maximumSize = new Dimension(width, height)
  targetImageLabel.icon = new ImageIcon(image)
  private val run = new Button("Run")

  var generation: Int = 0
  var topFitness = Double.NegativeInfinity

  var generationLabel = new Label(s"Generation $generation")
  var generationInfo = new TextArea("No Information")
  val leftPanel = new BoxPanel(Orientation.Vertical) {
    contents += generationLabel
    contents += generationBestImageLabel
    contents += generationInfo
    contents += run
  }

  val graphBox = new BoxPanel(Orientation.Horizontal) {
    contents += chartComp
  }

  val globalAndTarget = new BoxPanel(Orientation.Horizontal) {
    contents += globalBestImageLabel
    contents += targetImageLabel
  }
  val rightPanel = new BoxPanel(Orientation.Vertical) {
    contents += globalAndTarget
    contents += graphBox
  }

  private val mainPanel = new BoxPanel(Orientation.Horizontal) {
    contents += leftPanel
    contents += rightPanel
  }
  contents = mainPanel


  listenTo(run)

  reactions += {
    case ButtonClicked(`run`) => {
      incrementGeneration()

      image.getGraphics.fillOval(Random.nextInt(width), Random.nextInt(height), 5, 5)
      generationBestImageLabel.icon = new ImageIcon(image)

      val randomDouble = Random.nextDouble
      addToChart(randomDouble)

      mainPanel.revalidate
      mainPanel.repaint
    }
  }

  private def addToChart(randomDouble: Double) = {
    data.append((data.length, randomDouble))
    graphBox.contents -= chartComp
    chartComp = XYLineChart(data).toComponent
    graphBox.contents += chartComp
  }

  private def incrementGeneration() = {
    generation += 1
    leftPanel.contents -= generationLabel
    generationLabel = new Label(s"Generation $generation")
    leftPanel.contents.prepend(generationLabel)
  }

  def generationBestImage(image: BufferedImage, fitness: Double): Unit = {
    incrementGeneration()
    generationBestImageLabel.icon = new ImageIcon(image)
    addToChart(fitness)
    if (fitness > topFitness) {
      topFitness = fitness
      globalBestImageLabel.icon = new ImageIcon(image)
    }

    generationInfo.text = s"Global Best Fitness: $topFitness\nGeneration Best fitness: $fitness"


    mainPanel.revalidate
    mainPanel.repaint
  }
}
