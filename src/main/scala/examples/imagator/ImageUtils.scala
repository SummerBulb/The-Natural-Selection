package examples.imagator

import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO

/**
  * Created by SummerBulb on 10/11/2017.
  */
object ImageUtils {

  def openAndResize(path: String, resizeFactor: Double): BufferedImage = {
    val inputImage = ImageIO.read(new File(path))
    val IMG_WIDTH = Math.floor(inputImage.getWidth * resizeFactor).toInt
    val IMG_HEIGHT = Math.floor(inputImage.getHeight * resizeFactor).toInt

    val resizedImage = new BufferedImage(IMG_WIDTH, IMG_HEIGHT, inputImage.getType)
    val g = resizedImage.createGraphics
    g.drawImage(inputImage, 0, 0, IMG_WIDTH, IMG_HEIGHT, null)
    resizedImage
  }
}