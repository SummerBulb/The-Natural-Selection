package genetic

import examples.cube.Runner

import scala.util.Random

/**
  * Created by SummerBulb on 7/June/2018.
  */
class CharCreature(genes: Seq[Char]) extends Creature[Char](genes){
}

object CharCreature{
  def apply(genes: Seq[Char]): CharCreature = new CharCreature(genes)

}

