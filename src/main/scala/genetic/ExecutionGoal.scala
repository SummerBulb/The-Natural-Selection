package genetic

/**
  * Created by SummerBulb on 2/Oct/2017.
  */
trait ExecutionGoal[T, C <: CreatureLike[T, C], X <: C] {
  def satisfied(population: Population[T, C, X], age: Int): Boolean
}

case class FitnessGoal[T, C <: CreatureLike[T, C], X <: C](value: Double, fitnessFunction: Seq[T] => Double) extends ExecutionGoal[T, C, X] {
  override def satisfied(population: Population[T, C, X], age: Int): Boolean = {
    population.creatures.exists(c => c.fitness(fitnessFunction) == value)
  }
}

case class GenerationAgeGoal[T, C <: CreatureLike[T, C], X <: C](value: Int) extends ExecutionGoal[T, C, X] {
  override def satisfied(population: Population[T, C, X], age: Int): Boolean = {
    age >= value
  }
}
