package genetic.printer

import examples.cube.FoldCreature
import genetic.Creature

case class CubePrinter(fitnessFunction: Seq[Char] => Double, callback:FoldCreature => Unit) extends GenerationPrinter[Creature[Char],FoldCreature] {

  var bestScore = Double.MinValue
  val stringCreaturePrinter = StringCreaturePrinter(fitnessFunction)

  override def printGeneration(creatures: Seq[FoldCreature], generationAge: Int): Unit = {
    stringCreaturePrinter.printGeneration(creatures,generationAge)

    val bestFold = creatures.sortBy((creature) => creature.fitness(fitnessFunction))
      .head
    if (bestFold._fitness > bestScore) {
      bestScore = bestFold._fitness
      callback(bestFold)
    }
  }
}
