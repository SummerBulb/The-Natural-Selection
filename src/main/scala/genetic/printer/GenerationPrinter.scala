package genetic.printer

import java.awt.image.BufferedImage
import java.io.{File, FileWriter}
import java.nio.file.{Files, Paths}

import javax.imageio.ImageIO
import genetic.{Creature, CreatureLike}
import genetic.Utils.stubFitnessFunction


/**
  * Created by SummerBulb on 10/3/2017.
  */
trait GenerationPrinter[C <: CreatureLike[_,C], X <: C] {
  def printGeneration(creatures: Seq[X], generationAge: Int): Unit

}



case class StringCreaturePrinter(fitnessFunction: Seq[Char] => Double) extends GenerationPrinter[Creature[Char], Creature[Char]] {
  override def printGeneration(creatures: Seq[Creature[Char]], generationAge: Int): Unit = {
    println(s"Generation $generationAge")
    println("--------------")
    creatures
      .sortBy(c => c.fitness(fitnessFunction))
      .foreach(cr =>
        println(f"Fitness: ${cr.fitness(fitnessFunction)}%1.3f $cr%s.")
      )
    println("--------------")
  }
}

case class IntCreaturePrinter(fitnessFunction: Seq[Int] => Double) extends GenerationPrinter[Creature[Int], Creature[Int]] {
  override def printGeneration(creatures: Seq[Creature[Int]], generationAge: Int): Unit = {
    println(s"Generation $generationAge")
    println("--------------")
    creatures
      .sortBy(c => c.fitness(fitnessFunction))
      .foreach(creature =>
        println(f"Fitness: ${creature.fitness(fitnessFunction)}%1.3f ${creature.genes.mkString}%s.")
      )
    println("--------------")
  }
}


//case class ImageCreaturePrinter[T](fitnessFunction: Seq[T] => Double,
//                                   outputPath: String,
//                                   imageGenerator: Seq[T] => BufferedImage,
//                                   imagesToSave: Int) extends GenerationPrinter[T] {
//
//
//  override def printGeneration(creatures: Seq[CreatureLike[T,_]], generationAge: Int): Unit = {
//    println(s"Generation ${generationAge}. Saving images...")
//    val generationPath = outputPath + File.separator + generationAge
//    Files.createDirectories(Paths.get(generationPath))
//    val writer = new FileWriter(generationPath + File.separator + "stats.txt")
//    creatures
//      .sortBy((creature: CreatureLike[T,_]) => creature.fitness(stubFitnessFunction))
//      .reverse
//      .zipWithIndex
//      .foreach((tuple: (CreatureLike[T,_], Int)) => {
//        writer.write(tuple._1.fitness(stubFitnessFunction).toString + System.lineSeparator)
//        if (tuple._2 < imagesToSave) {
//          val path = generationPath + File.separator + tuple._2 + ".jpg"
//          val image = imageGenerator(tuple._1.genes)
//          ImageIO.write(image, "jpg", new File(path))
//        }
//      })
//
//    writer.close()
//    println("Done.")
//
//  }
//}