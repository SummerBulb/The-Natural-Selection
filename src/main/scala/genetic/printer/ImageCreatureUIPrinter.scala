package genetic.printer

import java.awt.image.BufferedImage
import java.io.{File, FileWriter}
import java.nio.file.{Files, Paths}
import javax.imageio.ImageIO

import genetic.Creature
import genetic.Utils.stubFitnessFunction
import examples.imagator.UI

/**
  * Created by SummerBulb on 10/16/2017.
  */
//case class StringCreaturePrinter(fitnessFunction: Seq[Char] => Double) extends GenerationPrinter[Creature[Char]] {}
case class ImageCreatureUIPrinter[T](fitnessFunction: Seq[T] => Double,
                                outputPath: String,
                                imageGenerator: Seq[T] => BufferedImage,
                                ui: UI,
                                imagesToSave: Int) extends GenerationPrinter[Creature[T],Creature[T]] {


  override def printGeneration(creatures: Seq[Creature[T]], generationAge: Int): Unit = {
    /**
      *
      * This is all just a copy-paste from ImageCreaturePrinter.
      * It needs an entire re-write. :D 👍
      *
      */
    println(s"Generation ${generationAge}. Saving images...")
    val generationPath = outputPath + File.separator + generationAge
    Files.createDirectories(Paths.get(generationPath))
    val writer = new FileWriter(generationPath + File.separator + "stats.txt")
    val sortedCreatures = creatures
      .sortBy(creature => creature.fitness(stubFitnessFunction))
      .reverse
    sortedCreatures
      .zipWithIndex
      .foreach((tuple: (Creature[T], Int)) => {
        writer.write(tuple._1.fitness(stubFitnessFunction).toString + System.lineSeparator)
        if (tuple._2 < imagesToSave) {
          val path = generationPath + File.separator + tuple._2 + ".jpg"
          val image = imageGenerator(tuple._1.genes)
          ImageIO.write(image, "jpg", new File(path))
        }
      })

    writer.close()
    println("Done.")

    val generationTopCreature = sortedCreatures.head
    val fitness = generationTopCreature.fitness(stubFitnessFunction)
    val image = imageGenerator(generationTopCreature.genes)
    ui.generationBestImage(image, fitness)

  }

}
