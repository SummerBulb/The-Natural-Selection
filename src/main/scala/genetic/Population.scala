package genetic

import scala.concurrent.ExecutionContext
import scala.util.Random

/**
  * Created by SummerBulb on 9/24/2017.
  */

case class Population[T, C <: CreatureLike[T, C], X <: C](val creatures: Seq[X], fitnessFunction: Seq[T] => Double, selector: Selection[T,C, X]) {

  def size() = creatures.length

  def select(selectionSize: Int): Population[T, C, X] = {
    val newGenerationCreatures = selector.select(creatures, selectionSize)
    Population[T, C, X](newGenerationCreatures, fitnessFunction, selector)
  }


  def reproduce(maxSize: Int, crossCutPosition: Int): Population[T, C, X] = {
    (size < maxSize) match {
      case true =>
        val newCreatures: Seq[X] = (size + 1 to maxSize).map { _ =>
          val mother = creatures(Random.nextInt(size))
          val father = creatures(Random.nextInt(size))
          father.mate(mother, crossCutPosition).asInstanceOf[X]
        }
        Population[T, C, X](creatures ++ newCreatures, fitnessFunction, selector)
      case false =>
        this
    }
  }

  def mutate(mutationRate: Double, mutator: T => T): Population[T, C, X] = {
    Population(
      creatures.map(creature => creature.mutate(mutationRate, mutator).asInstanceOf[X]),
      fitnessFunction,
      selector
    )
  }

  implicit val executionContext: ExecutionContext = ExecutionContext.global

  def calcFitness(): Population[T, C, X] = {
    creatures.par.foreach(c => c.fitness(fitnessFunction))

    this
  }

}
