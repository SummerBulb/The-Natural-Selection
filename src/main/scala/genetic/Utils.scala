package genetic

/**
  * Created by SummerBulb on 10/16/2017.
  */
object Utils {

  def stubFitnessFunction[T](stub: Seq[T]): Double = {
    throw new IllegalStateException(
      "This point should not have been reached. fitnessCalc() must be called before selection."
    )
    0
  }
}
