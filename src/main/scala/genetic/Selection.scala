package genetic

import genetic.Utils.stubFitnessFunction

import scala.util.Random

/**
  * Created by SummerBulb on 10/14/2017.
  */
trait Selection[T, C <: CreatureLike[T,C], X <: C] {
  def select(creatures: Seq[X], selectionSize: Int): Seq[X]
}

object Selection {

}

case class TopRanked[T, C <: CreatureLike[T,C], X <: C]() extends Selection[T,C,X] {
  def select(creatures: Seq[X], selectionSize: Int): Seq[X] = {
    creatures
      .sortBy(aCreature => aCreature.fitness(stubFitnessFunction[T]))
      .reverse
      .take(selectionSize)
  }
}

case class DiverseTopRanked[T, C <: CreatureLike[T,C], X <: C](distanceMeasure: (X, X) => Double,
                                                     averageCreature: Seq[X] => X,
                                                     maxFitness: Double,
                                                     maxDiversity: Double) extends Selection[T,C,X] {
  override def select(creatures: Seq[X], selectionSize: Int): Seq[X] = {
    val bestCreature = creatures.sortBy(c => c.fitness(stubFitnessFunction)).last
    var prev = creatures diff Seq(bestCreature)
    var selected = Seq(bestCreature)

    while (selected.size < selectionSize) {
      val avgCreature = averageCreature(selected)
      val rankedCreatures = creatures.map(creature =>
        (creature, creature.fitness(stubFitnessFunction[T]) / maxFitness, distanceMeasure(creature, avgCreature) / maxDiversity))

      val (currMaxFitness, currMaxDiversity) = rankedCreatures.foldLeft((Double.MinValue, Double.MinValue)) {
        (acc, curr) => (Math.max(acc._1, curr._2), Math.max(acc._2, curr._3))
      }

      val nextCreature = rankedCreatures
        .sortBy((tuple: (X, Double, Double)) => {

          val x = currMaxFitness - tuple._2
          val y = currMaxDiversity - tuple._3
          x * x + y * y
        })
        .head
        ._1

      selected = selected :+ nextCreature
      prev = creatures diff Seq(nextCreature)
    }

    selected
  }

  def diversity(creature: X, cluster: Seq[X]): Double = {
    cluster.size match {
      case 0 => 0
      case _ =>
        cluster
          .map(item => distanceMeasure(creature, item))
          .min
    }
  }
}

case class FitnessWeightedRandom[T, C <: CreatureLike[T,C], X <: C]() extends Selection[T,C,X] {
  def select(creatures: Seq[X], selectionSize: Int): Seq[X] = {
    val fitnesses = creatures.map(c => c.fitness(stubFitnessFunction[T]))
    val sum = fitnesses.sum
    val parentsStream = randomParentsStream(creatures, sum)

    parentsStream.take(selectionSize)
  }


  def randomParentsStream(creatures: Seq[X], fitnessSum: Double): Stream[X] = {
    Stream.continually(Random.nextInt(creatures.size))
      .filter(idx => {
        val score = creatures(idx).fitness(stubFitnessFunction[T]) + 1
        (score / (fitnessSum + 1)) > Random.nextDouble
      })
      .map(creatures(_))
  }
}