package genetic

import examples.cube.{FoldCreature, Runner}

import scala.util.Random

/**
  * Created by SummerBulb on 9/24/2017.
  */
class Creature[T](val genes: Seq[T]) extends CreatureLike[T, Creature[T]] {

  var _fitness: Double = -1

  def fitness(function: Seq[T] => Double): Double = {
    if (_fitness == -1)
      _fitness = function(genes)
    _fitness
  }

  def unfitness(function: Seq[T] => Double): Double = {
    ???
  }

  override def mate(other: Creature[T], cutPosition: Int) = {
    val position = Random.nextInt(genes.size)
    //val ts = genes.zip(other.genes).map(pair => if (Random.nextBoolean()) pair._1 else pair._2)
    val start = genes.take(position)
    val end = other.genes.drop(position)

    Creature[T](start ++ end)
  }

  def mutateGenes(mutationRate: Double, mutator: T => T): Seq[T] = {
    if (mutationRate < 0 || mutationRate > 1) {
      throw new IllegalArgumentException("Mutation rate must be between 0 and 1.")
    }

    val newGenes = genes.map((gene: T) =>
      if (Random.nextDouble() < mutationRate) mutator(gene) else gene
    )

    //    new C[T](newGenes)
    newGenes
  }

  def newCreature(newGenes: Seq[T]) = {
    Creature(newGenes)
  }

  // FIXME: This is incorrect. The method returns a Creature[T], but should return C <: Creature[T]
  def mutate(mutationRate: Double, mutator: T => T): Creature[T] = {
    val newGenes = mutateGenes(mutationRate, mutator)
    newCreature(newGenes)
  }

  override def toString: String = {
    s"Creature. Genes: ${genes.mkString(",")}"
  }
}

object Creature {
  def apply[T](genes: Seq[T]): Creature[T] = {
    new Creature(genes)
  }
}