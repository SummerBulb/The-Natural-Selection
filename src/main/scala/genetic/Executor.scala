package genetic

import genetic.printer.GenerationPrinter

/**
  * Created by SummerBulb on 9/27/2017.
  */
case class Executor[T, C <: CreatureLike[T, C], X <: C](
                                                         genesGenerator: () => X,
                                                         fitnessFunction: Seq[T] => Double,
                                                         executionGoal: ExecutionGoal[T, C, X],
                                                         mutator: T => T,
                                                         selector: Selection[T, C, X],
                                                         crossCutPosition: Int,
                                                         printer: GenerationPrinter[C, X],
                                                         maxPopulation: Int = 300,
                                                         selectionSize: Int = 200,
                                                         mutationRate: Double = 0.5
                                                       ) {
  var generationAge = 0

  lazy val firstGeneration = for (i <- 0 to maxPopulation) yield genesGenerator()
  var generation: Population[T, C, X] = null

  def isInitialized() = generationAge != 0

  def init() = {
    if (generationAge == 0) {
      generation = Population[T, C, X](firstGeneration, fitnessFunction, selector)
        .calcFitness()
      generationAge += 1
      printer.printGeneration(generation.creatures, generationAge)
    }
  }

  def step(steps: Int = 1) = {
    (1 to steps).foreach(_ =>
      if (generationAge == 0) {
        init()
      } else {
        generation = generation
          .select(selectionSize)
          .reproduce(maxPopulation, crossCutPosition)
          .mutate(mutationRate, mutator)
          .calcFitness()

        generationAge += 1
        printer.printGeneration(generation.creatures, generationAge)
      })

  }


  def run(): X = {
    if (generationAge == 0)
      init()

    while (!goalSatisfied) {
      step()
    }

    strongestCreature
  }

  def strongestCreature(): X = {
    generation.creatures.maxBy(_.fitness(fitnessFunction))
  }

  def weakestCreature(): X = {
    generation.creatures.minBy(_.fitness(fitnessFunction))
  }

  def goalSatisfied: Boolean = {
    executionGoal.satisfied(generation, generationAge)
  }
}