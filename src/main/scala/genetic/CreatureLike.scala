package genetic

trait CreatureLike[T, C <: CreatureLike[T,C]] {

  val genes: Seq[T]
  def newCreature(newGenes: Seq[T]): C

  def fitness(function: Seq[T] => Double): Double

  def mutate(mutationRate: Double, mutator: T => T): C

  def mate(other: C, cutPosition: Int): C

}
